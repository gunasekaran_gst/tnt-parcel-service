<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class Manual_deleteController extends Controller {
    public function index(){
        $users = DB::select('select * from manual');
        return view('manual/manual_delete',['users'=>$users]);
    }
    public function destroy($id) {
        DB::delete('delete from manual where  id= ?',[$id]);
        return redirect()->back()->with ('message',' Manual Booking Details Deleted ');
    }
}