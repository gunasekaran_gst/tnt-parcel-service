<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ReceiverController extends Controller {
    public function index(){
        $users = DB::select('select * from paid');
        return view('serial',['users'=>$users]);
    }
    public function show($id) {
        $users = DB::select('select * from paid where id = ?',[$id]);
        return view('receiver',['users'=>$users]);
    }
    public function edit(Request $request,$id) {
        $id = $request->id;
        $invoicedob = $request->invoicedob;
        $consignername = $request->consignername;
        $congname = $request->congname;
        $destination = $request->destination;
        $branch = $request->branch;
        $typeofpacking = $request->typeofpacking;
        $pkgs = $request->pkgs;
        $weight = $request->weight;
        $amount = $request->amount;
        $freight = $request->freight;
        $grandtotal = $request->grandtotal;

        DB::table('paid')
            ->where('id', $id)
            ->update(['id' => $id,'invoicedob' => $invoicedob,'consignername' => $consignername,'congname' => $congname,'destination' => $destination,'branch' => $branch,'typeofpacking' => $typeofpacking,'pkgs' => $pkgs,'weight' => $weight, 'amount' => $amount, 'freight' => $freight, 'grandtotal' => $grandtotal]);

//        DB::update('update staff set name = ?, mobileno = ?, where staff_id = ?',[$name, $mobileno, $staff_id]);
        return redirect()->back()->with ('message',' Paid Booking details upadeted ');
    }
}