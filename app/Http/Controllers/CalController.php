<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
//use App\Http\Controllers\Validator;
use Validator;

class CalController extends Controller
{
    public function insertform()
    {
        return view('paidbooking');
    }

    /**
     * @param Request $request
     */

    protected function groupPageSettingsValidator(array $data)
    {
        return Validator::make($data, [
            'document' => 'required',
            'mobileno' => 'required',
            'destination' => 'required',
            'consignername' => 'required',
            'branch' => 'required',
            'tomobileno' => 'required',
            'congname' => 'required',
            'package' => 'required',
            'pkgs' => 'required',
            'typeofpacking' => 'required',
            'ewaybill' => 'required',
            'weight' => 'required',
            'amount' => 'required',
            'freight' => 'required',
            'total' => 'required',
            'grandtotal' => 'required',


        ]);
    }


    public function insert(Request $request)
    {

        $request->validate([
            'document' => 'required',
            'mobileno' => 'required',
            'destination' => 'required',
            'consignername' => 'required',
            'branch' => 'required',
            'tomobileno' => 'required',
            'congname' => 'required',
            'package' => 'required',
            'pkgs' => 'required',
            'typeofpacking' => 'required',
            'ewaybill' => 'required',
            'weight' => 'required',
            'amount' => 'required',
            'freight' => 'required',
            'total' => 'required',
            'grandtotal' => 'required',


        ]);


        $document = $request->document;
        $mobileno = $request->mobileno;
        $destination = $request->destination;
        $consignername = $request->consignername;
        $branch = $request->branch;
        $tomobileno = $request->tomobileno;
        $congname = $request->congname;
        $package = $request->package;
        $pkgs = $request->pkgs;
        $typeofpacking = $request->typeofpacking;
        $ewaybill = $request->ewaybill;
        $weight = $request->weight;
        $amount = $request->amount;
        $freight = $request->freight;
        $total = $request->total;
        $grandtotal = $request->grandtotal;

//        var_dump($request->all());die;
        DB::insert('insert into padidetails (document, mobileno, destination, consignername, branch, tomobileno, congname,  package, pkgs, typeofpacking, ewaybill, weight, amount, freight, total, grandtotal) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [$document, $mobileno, $destination, $consignername, $branch, $tomobileno, $congname, $package, $pkgs, $typeofpacking, $ewaybill, $weight, $amount, $freight, $total, $grandtotal]);

//          return view('traningclass');
//        echo "Record inserted successfully.";
        return redirect()->back()->with ('message','paid calculation Booking success ');

//
    }
}