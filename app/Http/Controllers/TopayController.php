<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
//use App\Http\Controllers\Validator;
use Validator;

class TopayController extends Controller
{
    public function insertform()
    {
        return view('topaybooking');
    }

    /**
     * @param Request $request
     */

    protected function groupPageSettingsValidator(array $data)
    {
        return Validator::make($data, [
            'document' => 'required',
            'mobileno' => 'required',
            'destination' => 'required',
            'consignername' => 'required',
            'branch' => 'required',
            'tomobileno' => 'required',
            'congname' => 'required',
            'address' => 'required',
            'pin' => 'required',
            'contactname' => 'required',
            'email' => 'required',
            'department' => 'required',
            'fax' => 'required',
            'congaddress' => 'required',
            'conpin' => 'required',
            'congername' => 'required',
            'congemail' => 'required',
            'congerdepartment' => 'required',
            'congerfax' => 'required',
            'contain' => 'required',
            'declaredvalued' => 'required',
            'invoiceno' => 'required',
            'invoicedob' => 'required',
            'remarks' => 'required',
            'package' => 'required',
            'pkgs' => 'required',
            'typeofpacking' => 'required',
            'ewaybill' => 'required',
            'weight' => 'required',
            'amount' => 'required',
            'freight' => 'required',
            'loading' => 'required',
            'doorpickup' => 'required',
            'doordelivery' => 'required',
            'extra' => 'required',
            'total' => 'required',
            'grandtotal' => 'required',


        ]);
    }


    public function insert(Request $request)
    {

        $request->validate([
            'document' => 'required',
            'mobileno' => 'required',
            'destination' => 'required',
            'consignername' => 'required',
            'branch' => 'required',
            'tomobileno' => 'required',
            'congname' => 'required',
            'address' => 'required',
            'pin' => 'required',
            'contactname' => 'required',
            'email' => 'required',
            'department' => 'required',
            'fax' => 'required',
            'congaddress' => 'required',
            'conpin' => 'required',
            'congername' => 'required',
            'congemail' => 'required',
            'congerdepartment' => 'required',
            'congerfax' => 'required',
            'contain' => 'required',
            'declaredvalued' => 'required',
            'invoiceno' => 'required',
            'invoicedob' => 'required',
            'remarks' => 'required',
            'package' => 'required',
            'pkgs' => 'required',
            'typeofpacking' => 'required',
            'ewaybill' => 'required',
            'weight' => 'required',
            'amount' => 'required',
            'freight' => 'required',
            'loading' => 'required',
            'doorpickup' => 'required',
            'doordelivery' => 'required',
            'extra' => 'required',
            'total' => 'required',
            'grandtotal' => 'required',


        ]);


        $document = $request->document;
        $mobileno = $request->mobileno;
        $destination = $request->destination;
        $consignername = $request->consignername;
        $branch = $request->branch;
        $tomobileno = $request->tomobileno;
        $congname = $request->congname;
        $address = $request->address;
        $pin = $request->pin;
        $contactname = $request->contactname;
        $email = $request->email;
        $department = $request->department;
        $fax = $request->fax;
        $congaddress = $request->congaddress;
        $conpin = $request->conpin;
        $congername = $request->congername;
        $congemail = $request->congemail;
        $congerdepartment = $request->congerdepartment;
        $congerfax = $request->congerfax;
        $contain = $request->contain;
        $declaredvalued = $request->declaredvalued;
        $invoiceno = $request->invoiceno;
        $invoicedob = $request->invoicedob;
        $remarks = $request->remarks;
        $package = $request->package;
        $pkgs = $request->pkgs;
        $typeofpacking = $request->typeofpacking;
        $ewaybill = $request->ewaybill;
        $weight = $request->weight;
        $amount = $request->amount;
        $freight = $request->freight;
        $loading = $request->loading;
        $doorpickup= $request->doorpickup;
        $doordelivery = $request->doordelivery;
        $extra = $request->extra;
        $total = $request->total;
        $grandtotal = $request->grandtotal;

//        var_dump($request->all());die;
        DB::insert('insert into topay (document, mobileno, destination, consignername, branch, tomobileno, congname, address, pin, contactname, email, department, fax, congaddress, conpin, congername, congemail, congerdepartment, congerfax, contain, declaredvalued, invoiceno, invoicedob, remarks, package, pkgs, typeofpacking, ewaybill, weight, amount, freight, loading, doorpickup, doordelivery, extra, total, grandtotal) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [$document, $mobileno, $destination, $consignername, $branch, $tomobileno, $congname, $address, $pin, $contactname, $email, $department, $fax, $congaddress, $conpin, $congername, $congemail, $congerdepartment, $congerfax, $contain, $declaredvalued, $invoiceno, $invoicedob, $remarks, $package, $pkgs, $typeofpacking, $ewaybill, $weight, $amount, $freight, $loading, $doorpickup, $doordelivery, $extra, $total, $grandtotal]);

//          return view('traningclass');
//        echo "Record inserted successfully.";
        return redirect()->back()->with ('message','Topay Booking success ');

//
    }
}