<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class Dispatch_updateController extends Controller {
    public function index(){
        $users = DB::select('select * from dispatch');
        return view('dispatch/dispatch_edit',['users'=>$users]);
    }
    public function show($id) {
        $users = DB::select('select * from dispatch where id = ?',[$id]);
        return view('dispatch/dispatch_update',['users'=>$users]);
    }
    public function edit(Request $request,$id) {
        $id = $request->id;
        $dispatchno = $request->dispatchno;
        $vehicleno= $request->vehicleno;
        $drivername = $request->drivername;
        $cleanername = $request->cleanername;
        $dispatchdate = $request->dispatchdate;
        $source = $request->source;
        $destination = $request->destination;

        DB::table('dispatch')
            ->where('id', $id)
            ->update(['id' => $id,'dispatchno' => $dispatchno,'vehicleno' => $vehicleno,'drivername' => $drivername,'cleanername' => $cleanername,'dispatchdate' => $dispatchdate,'source' => $source,'destination' => $destination]);

//        DB::update('update staff set name = ?, mobileno = ?, where staff_id = ?',[$name, $mobileno, $staff_id]);
        return redirect()->back()->with ('message',' Dispatch Booking details upadeted ');
    }
}