<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class Manual_updateController extends Controller {
    public function index(){
        $users = DB::select('select * from manual');
        return view('manual/manual_edit',['users'=>$users]);
    }
    public function show($id) {
        $users = DB::select('select * from manual where id = ?',[$id]);
        return view('manual/manual_update',['users'=>$users]);
    }
    public function edit(Request $request,$id) {
        $id = $request->id;
        $pkgs = $request->pkgs;
        $typeofpacking = $request->typeofpacking;
        $amount = $request->amount;
        $congname = $request->congname;
        $paid = $request->paid;
        $contactname = $request->contactname;
        $invoiceno = $request->invoiceno;
        $remarks = $request->remarks;
        $invoicedob = $request->invoicedob;
        $grandtotal = $request->grandtotal;

        DB::table('manual')
            ->where('id', $id)
            ->update(['id' => $id,'pkgs' => $pkgs,'paid' => $paid,'typeofpacking' => $typeofpacking,'amount' => $amount,'congname' => $congname,'contactname' => $contactname,'invoiceno' => $invoiceno, 'remarks' => $remarks, 'invoicedob' => $invoicedob, 'grandtotal' => $grandtotal]);

//        DB::update('update staff set name = ?, mobileno = ?, where staff_id = ?',[$name, $mobileno, $staff_id]);
        return redirect()->back()->with ('message',' Manual Booking details upadeted ');
    }
}