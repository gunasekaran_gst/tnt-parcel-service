@extends('layout.main')
@section('title', 'Office Center in Krishnagiri')
@section('keywords', 'Office Center in Krishnagiri')
@section('description', 'Office Center in Krishnagiri')
@section('content')

    <h2 class="student_subhead">
        <div class="paid_backcolor"><strong class="welcomekbas"> </strong>
            <ul class="nav navbar-nav welcomekbas">
                <li class="dropdown">
                    <a href="{{ url('/') }}" data-toggle="dropdown" class="welcomekbas">Booking Details<b
                                class="caret"></b> </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="{{ url('/paidbookingdetails') }}">Paid Booking Details <i
                                        class="icon-arrow-right"></i></a>
                        </li>
                        <li>
                            <a href="{{ url('/topaybookingdetails') }}">To-Pay Booking Details <i
                                        class="icon-arrow-right"></i></a>
                        </li>
                        <li>
                            <a href="{{ url('/manualdetails') }}">Manual Booking Details <i
                                        class="icon-arrow-right"></i></a>
                        </li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="{{ url('/') }}" data-toggle="dropdown" class="welcomekbass">Search<b
                                class="caret"></b> </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="{{ url('/user') }}">Serial No</a>
                        </li>
                        <li>
                            <a href="{{ url('/name') }}">Packing Name</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="{{ url('/') }}" data-toggle="dropdown" class="welcomekbass">Dispatch<b
                                class="caret"></b> </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="{{ url('/serial') }}">Dispatch</a>
                        </li>
                        <li>
                            <a href="{{ url('/dispatch/dispatch_delete') }}">Delivery Details</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav">
                <li>
                    <a href="{{ url('/receiver/receiver_delete') }}" class="welcomekbass">Receiver details</a>
                </li>
            </ul>

            <div class="right_logout">
                <a class="logout" href="{{ url('/paidbooking') }}">PAID</a>
                <a class="logout" href="{{ url('/topaybooking') }}">TO PAY</a>
                <a class="logout" href="{{ url('/manualbooking') }}">MANUAL</a>
                <a class="logout" href="login"><strong>Logout</strong></a>
            </div>
        </div>
    </h2>


    <div class="container-fluid">
        <div class="container table_size">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h6>Delivery booking Details</h6>
                    <div class="container table_size">
                        <table bgcolor="#bdb76b" border ="8" class="table">
                            <thead>
                            <tr class="boldtable" bgcolor="#d3d3d3">
                                <td>Serial No</td>
                                <td>Consigner Name </td>
                                <td>Consigne Name </td>
                                <td>Type of Packing </td>
                                <td>Pkgs</td>
                                <td>Weight </td>
                                <td>Amount</td>
                                <td>Freight </td>
                                <td>Grand Total </td>
                                <td>Bill No</td>
                                <td>Vehicle No </td>
                                <td>Driver Name </td>
                                <td>Cleaner Name </td>
                                <td>Dispatch Date </td>
                                <td>Source </td>
                                <td>Destination </td>
                            </tr>
                            </thead>
                            @foreach ($users as $user)
                                <tr>
                                    <td>{{ $user->id }}</td>
                                    <td>{{ $user->consignername }}</td>
                                    <td>{{ $user->congname }}</td>
                                    <td>{{ $user->typeofpacking }}</td>
                                    <td>{{ $user->pkgs }}</td>
                                    <td>{{ $user->weight }}</td>
                                    <td>{{ $user->amount }}</td>
                                    <td>{{ $user->freight }}</td>
                                    <td>{{ $user->grandtotal }}</td>
                                    <td>{{ $user->dispatchno }}</td>
                                    <td>{{ $user->vehicleno }}</td>
                                    <td>{{ $user->drivername }}</td>
                                    <td>{{ $user->cleanername }}</td>
                                    <td>{{ $user->dispatchdate }}</td>
                                    <td>{{ $user->source }}</td>
                                    <td>{{ $user->destination }}</td>
                                    {{--<td><a href = 'seleced/{{ $user->id }}'button class="button button5">Purchase</a></td>--}}
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <h1>
        <table>
            <tr>
                <a href ="/dispatch"><button class="buttons buttons2">Dispatch</button></a>
                <a href ="dispatch/dispatch_delete"><button class="buttons buttons2">Delete</button></a>
                <a href ="dispatch/dispatch_edit"><button class="buttons buttons2">update</button></a>
                {{--<a href ="/seleced"><button class="button button2">Select</button></a>--}}
                </td>
            </tr>
        </table>
    </h1>

@endsection