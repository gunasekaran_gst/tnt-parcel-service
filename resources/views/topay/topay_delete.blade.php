@extends('layout.main')
@section('title', 'Office Center in Krishnagiri')
@section('keywords', 'Office Center in Krishnagiri')
@section('description', 'Office Center in Krishnagiri')
@section('content')

    <h2 class="student_subhead"> <div class="paid_backcolor"><strong class="welcomekbas">  </strong>
            <ul class="nav navbar-nav welcomekbas">
                <li class="dropdown">
                    <a href="{{ url('/') }}" data-toggle="dropdown" class="welcomekbas">Booking Details<b
                                class="caret"></b> </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a  href="{{ url('/paidbookingdetails') }}">Paid Booking Details <i class="icon-arrow-right"></i></a>
                        </li>
                        <li>
                            <a  href="{{ url('/topaybookingdetails') }}">To-Pay Booking Details <i class="icon-arrow-right"></i></a>
                        </li>
                        <li>
                            <a  href="{{ url('/manualdetails') }}">Manual Booking Details <i class="icon-arrow-right"></i></a>
                        </li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="{{ url('/') }}" data-toggle="dropdown" class="welcomekbass">Search<b
                                class="caret"></b> </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a  href="{{ url('/user') }}">WB No</a>
                        </li>
                        <li>
                            <a  href="{{ url('/name') }}">Type of Packing</a>
                        </li>
                    </ul>
                </li>
            </ul>

            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="{{ url('/') }}" data-toggle="dropdown" class="welcomekbass">Dispatch<b
                                class="caret"></b> </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a  href="{{ url('/dispatch') }}">Dispatch Reg</a>
                        </li>
                        <li>
                            <a  href="{{ url('/dispatchdetails') }}">Dispatch Details</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <div class="right_logout">
                <a class="logout" href="{{ url('/paidbooking') }}">PAID</a>
                <a  class="logout" href="{{ url('/topaybooking') }}">TO PAY</a>
                <a  class="logout" href="{{ url('/manualbooking') }}">MANUAL</a>
                <a class="logout" href ="login"><strong>Logout</strong></a>
            </div>
        </div>
    </h2>
    <h4>
        <center style="color:red;">
            @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif
        </center>
    </h4>


    <div class="container-fluid">
        <div class="container table_size">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h6>Topay booking Details</h6>
                    <div class="container table_size">
                        <table bgcolor="#bdb76b" border ="8" class="table">
                            <thead>
                            <tr class="boldtable" bgcolor="#d3d3d3">
                                <td>WB No</td>
                                <td>Receiver No. </td>
                                <td>Booking Date </td>
                                <td>Destination </td>
                                <td>Wb type </td>
                                <td>Sender </td>
                                <td>Receiver </td>
                                <td>Origine(Branch) </td>
                                <td>Remark </td>
                                <td>Pkgs</td>
                                <td>Amt </td>
                                <td>Grand Total </td>
                            </tr>
                            </thead>
                            @foreach ($users as $user)
                                <tr>
                                    <td>{{ $user->id }}</td>
                                    <td>{{ $user->invoiceno }}</td>
                                    <td>{{ $user->invoicedob }}</td>
                                    <td>{{ $user->destination }}</td>
                                    <td>{{ $user->typeofpacking }}</td>
                                    <td>{{ $user->congname }}</td>
                                    <td>{{ $user->contactname }}</td>
                                    <td>{{ $user->branch }}</td>
                                    <td>{{ $user->remarks }}</td>
                                    <td>{{ $user->pkgs }}</td>
                                    <td>{{ $user->amount }}</td>
                                    <td>{{ $user->grandtotal }}</td>
                                    <td> <a href = 'delete/{{ $user->id }}' button class="buttons buttons5"> Delete </a>  </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection