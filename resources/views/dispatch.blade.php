@extends('layout.main')
@section('title', 'Office Center in Krishnagiri')
@section('keywords', 'Office Center in Krishnagiri')
@section('description', 'Office Center in Krishnagiri')
@section('content')

    <h2 class="student_subhead">
        <div class="paid_backcolor"><strong class="welcomekbas"> </strong>
            <ul class="nav navbar-nav welcomekbas">
                <li class="dropdown">
                    <a href="{{ url('/') }}" data-toggle="dropdown" class="welcomekbas">Booking Details<b
                                class="caret"></b> </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="{{ url('/paidbookingdetails') }}">Paid Booking Details <i
                                        class="icon-arrow-right"></i></a>
                        </li>
                        <li>
                            <a href="{{ url('/topaybookingdetails') }}">To-Pay Booking Details <i
                                        class="icon-arrow-right"></i></a>
                        </li>
                        <li>
                            <a href="{{ url('/manualdetails') }}">Manual Booking Details <i
                                        class="icon-arrow-right"></i></a>
                        </li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="{{ url('/') }}" data-toggle="dropdown" class="welcomekbass">Search<b
                                class="caret"></b> </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="{{ url('/user') }}">Serial No</a>
                        </li>
                        <li>
                            <a href="{{ url('/name') }}">Packing Name</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="{{ url('/') }}" data-toggle="dropdown" class="welcomekbass">Dispatch<b
                                class="caret"></b> </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="{{ url('/serial') }}">Dispatch</a>
                        </li>
                        <li>
                            <a href="{{ url('/dispatch/dispatch_delete') }}">Delivery Details</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav">
                <li>
                    <a href="{{ url('/receiver/receiver_delete') }}" class="welcomekbass">Receiver details</a>
                </li>
            </ul>

            <div class="right_logout">
                <a class="logout" href="{{ url('/paidbooking') }}">PAID</a>
                <a class="logout" href="{{ url('/topaybooking') }}">TO PAY</a>
                <a class="logout" href="{{ url('/manualbooking') }}">MANUAL</a>
                <a class="logout" href="login"><strong>Logout</strong></a>
            </div>
        </div>
    </h2>

    <h4>
        <center style="color:red;">
            @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif
        </center>
    </h4>

    <div class="container-fluid">
        <div class="container table_size">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h6>Destination City Details</h6>
                    <div class="container table_size">
                        <table bgcolor="#bdb76b" border ="8" class="table">
                            <thead>
                            <tr class="boldtable" bgcolor="#d3d3d3">
                                <td>Serial No</td>
                                <td>Booking Date </td>
                                <td>Consigner Name </td>
                                <td>Consigne Name </td>
                                <td>Destination City </td>
                                <td>Branch </td>
                                <td>Type of Packing </td>
                                <td>Pkgs</td>
                                <td>Weight </td>
                                <td>Amount</td>
                                <td>Freight </td>
                                <td>Grand Total </td>


                            </tr>
                            </thead>
                            @foreach ($users as $user)
                                <tr>
                                    <td>{{ $user->id }}</td>
                                    <td>{{ $user->invoicedob }}</td>
                                    <td>{{ $user->consignername }}</td>
                                    <td>{{ $user->congname }}</td>
                                    <td>{{ $user->destination }}</td>
                                    <td>{{ $user->branch }}</td>
                                    <td>{{ $user->typeofpacking }}</td>
                                    <td>{{ $user->pkgs }}</td>
                                    <td>{{ $user->weight }}</td>
                                    <td>{{ $user->amount }}</td>
                                    <td>{{ $user->freight }}</td>
                                    <td>{{ $user->grandtotal }}</td>
                                    <td> <a href = 'edit/{{ $user->id }}' button class="buttons buttons2"> Receive </a>  </td>
                                    <td> <a href = 'edits/{{ $user->id }}' button class="buttons buttons5"> Delivery </a>  </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <form action="/dispatch" method="post">
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
        <div class="container-fluid">
            <div class="container">
                <h6>Delivery Form:</h6>
                <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 padding_left_right_null padd_top_30">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 ">
                        <div class="sizes"><strong> Serial No</strong></div>
                            <input type='text' name='dispatchno'
                                   readonly  value='<?php echo $users[0]->id; ?>'/>

                        <div class="sizes"><strong> Booking Date</strong></div>
                        <input type='text' name='invoicedob'
                               readonly  value='<?php echo $users[0]->invoicedob; ?>'/>

                        <div class="sizes"><strong> Consigner Name</strong></div>
                        <input type='text' name='consignername'
                               readonly value='<?php echo $users[0]->consignername; ?>'/>

                        <div class="sizes"><strong>Consigne Name</strong></div>
                        <input type='text' name='congname'
                               readonly value='<?php echo $users[0]->congname; ?>'/>

                        <div class="sizes"><strong>Vehicle No</strong></div>
                        <fieldset class="{{ $errors->has('Vehicleno') ? ' has-error' : '' }}">
                            <input type="text" id="vehicleno" name="vehicleno"
                                   placeholder="Enter the  Vehicle no.....">
                            @if ($errors->has('vehicleno'))<span
                                    class="help-block error_font"><strong>{{ $errors->first('vehicleno') }}</strong></span>@endif
                        </fieldset>
                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="sizes"><strong>Destination City</strong></div>
                        <input type='text' name='destinationcity'
                               readonly    value='<?php echo $users[0]->destination; ?>'/>

                        <div class="sizes"><strong>Branch</strong></div>
                        <input type='text' name='branch'
                               readonly  value='<?php echo $users[0]->branch; ?>'/>

                        <div class="sizes"><strong> Type of Packing</strong></div>
                        <input type='text' name='typeofpacking'
                               readonly  value='<?php echo $users[0]->typeofpacking; ?>'/>

                        <div class="sizes"><strong>Pkgs</strong></div>
                        <input type='text' name='pkgs'
                               readonly  value='<?php echo $users[0]->pkgs; ?>'/>


                        <div class="sizes"><strong>Driver Name</strong></div>
                        <fieldset class="{{ $errors->has('drivername') ? ' has-error' : '' }}">
                            <input type="text" id="drivername" name="drivername"
                                   placeholder="Enter the  driver name.....">
                            @if ($errors->has('drivername'))<span
                                    class="help-block error_font"><strong>{{ $errors->first('drivername') }}</strong></span>@endif
                        </fieldset>


                        <div class="sizes"><strong>Cleaner Name:</strong></div>
                        <fieldset class="{{ $errors->has('cleanername') ? ' has-error' : '' }}">
                            <input type="text" id="cleanername" name="cleanername"
                                   placeholder="Enter the cleaner name....">
                            @if ($errors->has('cleanername'))<span
                                    class="help-block error_font"><strong>{{ $errors->first('cleanername') }}</strong></span>@endif

                        </fieldset>
                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 ">

                        <div class="sizes"><strong> Weight</strong></div>
                        <input type='text' name='weight'
                               readonly    value='<?php echo $users[0]->weight; ?>'/>

                        <div class="sizes"><strong> Amount</strong></div>
                        <input type='text' name='amount'
                               readonly  value='<?php echo $users[0]->amount; ?>'/>

                        <div class="sizes"><strong> Freight</strong></div>
                        <input type='text' name='freight'
                               readonly   value='<?php echo $users[0]->freight; ?>'/>

                        <div class="sizes"><strong> Grand Total</strong></div>
                        <input type='text' name='grandtotal'
                               readonly  value='<?php echo $users[0]->grandtotal; ?>'/>


                        <div class="sizes"><strong>Dispatch Date</strong></div>
                        <fieldset class="{{ $errors->has('dispatchdate') ? ' has-error' : '' }}">
                            <input type="date" id="dispatchdate" name="dispatchdate">
                            @if ($errors->has('dispatchdate'))<span
                                    class="help-block error_font"><strong>{{ $errors->first('dispatchdate') }}</strong></span>@endif
                        </fieldset>

                        <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 padding_left_right_null">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padding_left_right_null">
                                <div class="sizes"><strong>Source</strong></div>
                                <fieldset class="{{ $errors->has('source') ? ' has-error' : '' }}">
                                    <select id="source" name="source">
                                        <option value="">Select the City</option>
                                        <option value="Bangalore">Bangalore</option>
                                        <option value="Bhavani">Bhavani</option>
                                        <option value="Coimbatore">Coimbatore</option>
                                        <option value="Destination City">Destination City</option>
                                        <option value="Dindigul">Dindigul</option>
                                        <option value="Ellampillai">Ellampillai</option>
                                        <option value="Erode">Erode</option>
                                        <option value="Hosur">Hosur</option>
                                        <option value="Jalakandapuram">Jalakandapuram</option>
                                        <option value="Mecheri">Mecheri</option>
                                        <option value="Omalur">Omalur</option>
                                        <option value="Ooty">Ooty</option>
                                        <option value="Puliyampatti">Puliyampatti</option>
                                        <option value="Salem">Salem</option>
                                        <option value="Sathyamangalam">Sathyamangalam</option>
                                        <option value="Vellore">Vellore</option>
                                    </select>
                                    @if ($errors->has('source'))<span
                                            class="help-block error_font"><strong>{{ $errors->first('source') }}</strong></span>@endif
                                </fieldset>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padding_left_right_null">
                                <div class="sizes"><strong>Destination</strong></div>
                                <fieldset class="{{ $errors->has('destination') ? ' has-error' : '' }}">
                                    <select id="destination" name="destination">
                                        <option value="">Select the City</option>
                                        <option value="Bangalore">Bangalore</option>
                                        <option value="Bhavani">Bhavani</option>
                                        <option value="Coimbatore">Coimbatore</option>
                                        <option value="Destination City">Destination City</option>
                                        <option value="Dindigul">Dindigul</option>
                                        <option value="Ellampillai">Ellampillai</option>
                                        <option value="Erode">Erode</option>
                                        <option value="Hosur">Hosur</option>
                                        <option value="Jalakandapuram">Jalakandapuram</option>
                                        <option value="Mecheri">Mecheri</option>
                                        <option value="Omalur">Omalur</option>
                                        <option value="Ooty">Ooty</option>
                                        <option value="Puliyampatti">Puliyampatti</option>
                                        <option value="Salem">Salem</option>
                                        <option value="Sathyamangalam">Sathyamangalam</option>
                                        <option value="Vellore">Vellore</option>
                                    </select>
                                    @if ($errors->has('destination'))<span
                                            class="help-block error_font"><strong>{{ $errors->first('destination') }}</strong></span>@endif
                                </fieldset>
                            </div>
                        </div>
                    </div>

                </div>


                <div class="button_submits col-md-12 col-sm-12 col-xs-12 col-lg-12 padd_top_10">
                    <input type="submit" value="Submit">&nbsp;
                    <input type="reset" value="Clear">
                </div>

            </div>
        </div>
    </form>

    <h1>
        <table>
            <tr>
                <a href ="/dispatch"><button class="buttons buttons2">Dispatch</button></a>
                <a href ="dispatch/dispatch_delete"><button class="buttons buttons2">Delete</button></a>
                <a href ="dispatch/dispatch_edit"><button class="buttons buttons2">update</button></a>
                <a href ="receiver/receiver_edit"><button class="buttons buttons2">update</button></a>
                {{--<a href ="/seleced"><button class="button button2">Select</button></a>--}}
                </td>
            </tr>
        </table>
    </h1>

@endsection