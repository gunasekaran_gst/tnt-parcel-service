@extends('layout.main')
@section('title', 'Office Center in Krishnagiri')
@section('keywords', 'Office Center in Krishnagiri')
@section('description', 'Office Center in Krishnagiri')
@section('content')

    <h2 class="student_subhead">
        <div class="paid_backcolor"><strong class="welcomekbas"> </strong>
            <ul class="nav navbar-nav welcomekbas">
                <li class="dropdown">
                    <a href="{{ url('/') }}" data-toggle="dropdown" class="welcomekbas">Booking Details<b
                                class="caret"></b> </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="{{ url('/paidbookingdetails') }}">Paid Booking Details <i
                                        class="icon-arrow-right"></i></a>
                        </li>
                        <li>
                            <a href="{{ url('/topaybookingdetails') }}">To-Pay Booking Details <i
                                        class="icon-arrow-right"></i></a>
                        </li>
                        <li>
                            <a href="{{ url('/manualdetails') }}">Manual Booking Details <i
                                        class="icon-arrow-right"></i></a>
                        </li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="{{ url('/') }}" data-toggle="dropdown" class="welcomekbass">Search<b
                                class="caret"></b> </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="{{ url('/user') }}">Serial No</a>
                        </li>
                        <li>
                            <a href="{{ url('/name') }}">Packing Name</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="{{ url('/') }}" data-toggle="dropdown" class="welcomekbass">Dispatch<b
                                class="caret"></b> </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="{{ url('/serial') }}">Dispatch</a>
                        </li>
                        <li>
                            <a href="{{ url('/dispatch/dispatch_delete') }}">Delivery Details</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav">
                <li>
                    <a href="{{ url('/receiver/receiver_delete') }}" class="welcomekbass">Receiver details</a>
                </li>
            </ul>

            <div class="right_logout">
                <a class="logout" href="{{ url('/paidbooking') }}">PAID</a>
                <a class="logout" href="{{ url('/topaybooking') }}">TO PAY</a>
                <a class="logout" href="{{ url('/manualbooking') }}">MANUAL</a>
                <a class="logout" href="login"><strong>Logout</strong></a>
            </div>
        </div>
    </h2>

        <div class="container-fluid profile ">
            <div class="container">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                    </div>

                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 title">TNT Parcel Service</div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 subtitle">No 236c, Newpet, Near Daily Thanthi Office, Roundana Krishnagiri.</div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 subtitle">Krishnagiri - 635 001, Tamil Nadu, S.India</div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 subtitle padd_buttom_10">Phone :123 4567 890 Email:gmail.com</div>

                <table>
                    <tr>
                        <td><strong>Serial No: {{$login->id}}</strong></td>
                        <th class="invoice">Invoice</th>
                        <td><strong>Booking Date: {{$login->invoicedob}}</strong></td>
                    </tr>
                </table>
                    <table>
                        <tr>
                            <td><strong>Consigner (From)</strong><br>
                            Consigner Phone NO: <strong>{{$login->mobileno}}</strong><br>
                            Consigner Name: <strong>{{$login->consignername}}</strong></td>

                            <td><strong>Consigne (To)</strong><br>
                            Consigne Phone NO: <strong> {{$login->tomobileno}}</strong><br>
                           Consigne Name: <strong> {{$login->congname}}</strong></td>
                        </tr>
                        <tr>
                            <td>Destination: </td>
                            <td><strong> {{$login->destination}}</strong></td>
                        </tr>
                        <tr>
                            <td>Branch: </td>
                            <td><strong> {{$login->branch}}</strong></td>
                        </tr>
                        <tr>
                            <td>Weight: </td>
                            <td><strong> {{$login->weight}}</strong></td>
                        </tr>
                        <tr>
                            <td>No of Pkgs: </td>
                            <td><strong> {{$login->pkgs}}</strong></td>
                        </tr>
                        <tr>
                            <td>Type of Packing: </td>
                            <td><strong> {{$login->typeofpacking}}</strong></td>
                        </tr>
                        <tr>
                            <td>Amount: </td>
                            <td><strong> {{$login->amount}}</strong></td>
                        </tr>
                        <tr>
                            <td>Freight: </td>
                            <td><strong> {{$login->freight}}</strong></td>
                        </tr>
                        <tr>
                            <td>Total: </td>
                            <td><strong> {{$login->total}}</strong></td>
                        </tr>
                        <tr>
                            <td><strong>GrandTotal:</strong> </td>
                            <td><strong> {{$login->grandtotal}}</strong></td>
                        </tr>

                    </table>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <strong class="subtitles col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_top_30">Read and Accepted</strong><br>
                           <strong class="subtitles col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_top_30"> Issue date:
                               <?php echo date("d/m/y");?></strong>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <strong class="subtitles col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_top_two_em">Signature</strong><br>
                            <strong class="subtitles col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_top_two_em">..........................................</strong><br>
                        </div>
                    </div>

                </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                    </div>
                </div>

            </div>
        </div>


    <script>
        function myFunction() {
            window.print();
        }
    </script>

    @endsection








