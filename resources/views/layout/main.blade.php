<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content='@yield('keywords')'>
    <meta name="description" content='@yield('description')'>
    <title>Queries - @yield('title')</title>
    <link rel="stylesheet" href="{{URL::asset('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{URL::asset('css/style.css') }}">
    <link rel="stylesheet" href="{{URL::asset('css/content.css') }}">
    <link rel="stylesheet" href="{{URL::asset('css/animate.css') }}">
    <link rel="stylesheet" href="{{URL::asset('css/responsive.css') }}">
    <script src="{{URL::asset('js/jquery.js') }}"></script>
    <script src="{{URL::asset('js/bootstrap.js') }}"></script>
    <script src="{{URL::asset('js/wow.js') }}"></script>
    <script src="{{URL::asset('js/custom.js') }}"></script>

</head>
<body>

<div class="container-fluid header">
    <div class="container-fluid menu">
        <nav class="navbar-inverse custom_class">
            <div class="container-fluid">

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle side_menu_toggle" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="logo col-lg-12 col-md-12 col-sm-12 col-xs-9">
                        <div class="parcel">  TNT parcel service </div>
                        {{--<a href="{{ url('/') }}"> <img src="{{URL::asset('image/flogo.jpg') }}" width="300px" height="55px"> </a>--}}
                    </div>
                </div>

                <div class="meanu">
                    <div class="collapse navbar-collapse topmenu" id="myNavbar">
                        <ul class="nav navbar-nav">
                            <li><a  href="{{ url('/#') }}">Home</a></li>
                            <li><a  href="{{ url('/login') }}">Login</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
    </div>
</div>
@yield('content')
</body>
</html>

