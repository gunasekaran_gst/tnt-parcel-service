@extends('layout.main')
@section('title', 'Office Center in Krishnagiri')
@section('keywords', 'Office Center in Krishnagiri')
@section('description', 'Office Center in Krishnagiri')
@section('content')


    <h2 class="student_subhead">
        <div class="paid_backcolor"><strong class="welcomekbas"> </strong>
            <ul class="nav navbar-nav welcomekbas">
                <li class="dropdown">
                    <a href="{{ url('/') }}" data-toggle="dropdown" class="welcomekbas">Booking Details<b
                                class="caret"></b> </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="{{ url('/paidbookingdetails') }}">Paid Booking Details <i
                                        class="icon-arrow-right"></i></a>
                        </li>
                        <li>
                            <a href="{{ url('/topaybookingdetails') }}">To- Booking Details <i
                                        class="icon-arrow-right"></i></a>
                        </li>
                        <li>
                            <a href="{{ url('/manualdetails') }}">Manual Booking Details <i
                                        class="icon-arrow-right"></i></a>
                        </li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="{{ url('/') }}" data-toggle="dropdown" class="welcomekbass">Search<b
                                class="caret"></b> </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="{{ url('/user') }}">Serial No</a>
                        </li>
                        <li>
                            <a href="{{ url('/name') }}">Packing Name</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="{{ url('/') }}" data-toggle="dropdown" class="welcomekbass">Dispatch<b
                                class="caret"></b> </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="{{ url('/serial') }}">Dispatch</a>
                        </li>
                        <li>
                            <a href="{{ url('/dispatch/dispatch_delete') }}">Delivery Details</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav">
                <li>
                    <a href="{{ url('/receiver/receiver_delete') }}" class="welcomekbass">Receiver details</a>
                </li>
            </ul>

            <div class="right_logout">
                <a class="logout" href="{{ url('/paidbooking') }}">PAID</a>
                <a class="logout" href="{{ url('/topaybooking') }}">TO PAY</a>
                <a class="logout" href="{{ url('/manualbooking') }}">MANUAL</a>
                <a class="logout" href="login"><strong>Logout</strong></a>
            </div>
        </div>
    </h2>
    <h4>
        <center>
        @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
        </center>
<table>
    <tr>
        <th> Serial No:
            @foreach ($users as $user)
                {{ $user->id }}
            @endforeach
        </th>
    </tr>
</table>

    </h4>
    <form action="/paidbooking" method="post">
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
        <div class="container-fluid">
            <div class="container">
                <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 padding_left_right_null">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 ">
                        <div class="sizes"><strong>Document:</strong><span class="star-rating">*</span></div>
                        <fieldset class="{{ $errors->has('document') ? ' has-error' : '' }}">
                            <select id="document" name="document">
                                <option value="">Select the document</option>
                                <option value="nodocument"> No Document</option>
                                <option value="document">Document</option>
                            </select>
                            @if ($errors->has('document'))<span
                                    class="help-block error_font"><strong>{{ $errors->first('document') }}</strong></span>@endif
                        </fieldset>

                        <div class="sizes"><strong> Consigner (From)</strong><span class="star-rating">*</span></div>
                        <fieldset class="{{ $errors->has('mobileno') ? ' has-error' : '' }}">
                            <input type="text" id="mobileno" name="mobileno"
                                   placeholder="Enter the  Phone no....." value="{{ old('mobileno') }}">
                            @if ($errors->has('mobileno'))<span
                                    class="help-block error_font"><strong>{{ $errors->first('mobileno') }}</strong></span>@endif
                        </fieldset>

                        <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 padding_left_right_null padd_top_10 padd_buttom_20">

                            <div id="flips"> ADD Register</div>
                            <div id="panels">
                                <div class="sizes"><strong>Address:</strong>
                                </div>
                                <input type="text" id="pin" name="address" placeholder="Enter the  Pin no....."
                                       value="address">


                                <div class="sizes"><strong>PIN No:</strong></div>
                                <input type="text" id="pin" name="pin" placeholder="Enter the  Pin no....." value="pin">


                                <div class="sizes"><strong>Contact Name:</strong>
                                </div>

                                <input type="text" id="contactname" name="contactname"
                                       placeholder="Enter the Contact Name..." value="contactname">


                                <div class="sizes"><strong> E-mail address:</strong></div>
                                <input type="text" id="email" name="email"
                                       placeholder="Enter the Email Address....." value="email">


                                <div class="sizes"><strong>Department:</strong></div>

                                <input type="text" id="department" name="department"
                                       placeholder="Enter the Department ..." value="department">


                                <div class="sizes"><strong>FAX:</strong></div>

                                <input type="text" id="fax" name="fax" placeholder="Enter the Fax ....." value="fax">

                            </div>
                        </div>

                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">

                        <div class="sizes"><strong>Destination City:</strong><span class="star-rating">*</span></div>
                        <fieldset class="{{ $errors->has('destination') ? ' has-error' : '' }}">
                            <select id="destination" name="destination">
                                <option value="">Destination City</option>
                                <option value="bhavani">Bhavani</option>
                                <option value="coimbatore">Coimbatore</option>
                                <option value="dindigul">Dindigul</option>
                                <option value="ellampillai">Ellampillai</option>
                                <option value="erode">Erode</option>
                                <option value="hosur">Hosur</option>
                                <option value="jalakandapuram">Jalakandapuram</option>
                                <option value="mecheri">Mecheri</option>
                                <option value="omalur">Omalur</option>
                                <option value="puliyampatti">Puliyampatti</option>
                                <option value="salem">Salem</option>
                                <option value="sathyamangalam">Sathyamangalam</option>
                                <option value="vellore">Vellore</option>
                            </select>
                            @if ($errors->has('destination'))<span
                                    class="help-block error_font"><strong>{{ $errors->first('destination') }}</strong></span>@endif
                        </fieldset>


                        <div class="sizes"><strong>Consigner Name:</strong><span class="star-rating">*</span></div>
                        <fieldset class="{{ $errors->has('consignername') ? ' has-error' : '' }}">
                            <input type="text" id="consignername" name="consignername"
                                   placeholder="Enter the Consigner Name...." value="{{ old('consignername') }}">
                            @if ($errors->has('consignername'))<span
                                    class="help-block error_font"><strong>{{ $errors->first('consignername') }}</strong></span>@endif

                        </fieldset>
                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 ">

                        <div class="sizes"><strong>Select the Branch:</strong><span class="star-rating">*</span></div>
                        <fieldset class="{{ $errors->has('branch') ? ' has-error' : '' }}">
                            <select id="branch" name="branch">
                                <option value="select">Select the Branch</option>
                                <option value="bhavani">Bhavani</option>
                                <option value="coimbatore">Coimbatore</option>
                                <option value="dindigul">Dindigul</option>
                                <option value="ellampillai">Ellampillai</option>
                                <option value="erode">Erode</option>
                                <option value="hosur">Hosur</option>
                                <option value="jalakandapuram">Jalakandapuram</option>
                                <option value="mecheri">Mecheri</option>
                                <option value="omalur">Omalur</option>
                                <option value="puliyampatti">Puliyampatti</option>
                                <option value="salem">Salem</option>
                                <option value="sathyamangalam">Sathyamangalam</option>
                                <option value="vellore">Vellore</option>
                            </select>
                            @if ($errors->has('branch'))<span
                                    class="help-block error_font"><strong>{{ $errors->first('branch') }}</strong></span>@endif
                        </fieldset>

                        <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 padding_left_right_null">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padding_left_right_null">

                                <div class="sizes"><strong> Consigne (To)</strong><span class="star-rating">*</span>
                                </div>
                                <fieldset class="{{ $errors->has('mobileno') ? ' has-error' : '' }}">
                                    <input type="text" id="mobileno" name="tomobileno"
                                           placeholder="Enter the  Phone no....." value="{{ old('tomobileno') }}">
                                    @if ($errors->has('mobileno'))<span
                                            class="help-block error_font"><strong>{{ $errors->first('mobileno') }}</strong></span>@endif
                                </fieldset>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padding_left_right_null">

                                <div class="sizes"><strong>Consigne Name:</strong><span class="star-rating">*</span>
                                </div>
                                <fieldset class="{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <input type="text" id="name" name="congname" placeholder="Consigner Name...."
                                           value="{{ old('tomobileno') }}">
                                    @if ($errors->has('name'))<span
                                            class="help-block error_font"><strong>{{ $errors->first('name') }}</strong></span>@endif

                                </fieldset>
                            </div>

                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 padding_left_right_null padd_top_10 padd_buttom_20">

                            <div id="flip"> ADD Register</div>
                            <div id="panel">
                                <div class="sizes"><strong>Address:</strong>
                                </div>

                                <input type="text" id="pin" name="congaddress" placeholder="Enter the  Pin no....."
                                       value="congaddress">

                                <div class="sizes"><strong>PIN No:</strong></div>
                                <input type="text" id="pin" name="conpin" placeholder="Enter the  Pin no....."
                                       value="conpin">


                                <div class="sizes"><strong>Contact Name:</strong>
                                </div>
                                <input type="text" id="congername" name="congername"
                                       placeholder="Enter the Contact Name..." value="congername">


                                <div class="sizes"><strong> E-mail address:</strong>
                                </div>
                                <input type="text" id="congemail" name="congemail"
                                       placeholder="Enter the Email Address....." value="congemail">


                                <div class="sizes"><strong>Department:</strong></div>
                                <input type="text" id="congerdepartment" name="congerdepartment"
                                       placeholder="Enter the Department ..." value="congerdepartment">


                                <div class="sizes"><strong>FAX:</strong></div>
                                <input type="text" id="congerfax" name="congerfax"
                                       placeholder="Enter the Fax ....." value="congerfax">

                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                        <h4 class="student_subhead">
                            <div class="paid_backcolors consignemt">Consignment Details</div>
                        </h4>
                    </div>

                    <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 padding_left_right_null padd_buttom_10">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 ">

                            <div class="sizes"><strong>Said to Contain:</strong></div>
                            <input type="text" id="contain" name="contain"
                                   placeholder="Enter the Said to Contain...." value="contain">

                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">

                            <div class="sizes"><strong>Declared Valued:</strong></div>
                            <input type="text" id="declaredvalued" name="declaredvalued"
                                   placeholder="Enter the Consigner Name...." value="declaredvalued">

                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 ">

                            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 padding_left_right_null">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padding_left_right_null">

                                    <div class="sizes"><strong>Invoice No:</strong>
                                    </div>
                                    <input type="text" id="invoiceno" name="invoiceno"
                                           placeholder="Enter the Invoice no....." value="invoiceno">

                                </div>

                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padding_left_right_null">
                                    <div class="sizes"><strong>Remarks:</strong></div>
                                    <input type="text" id="remarks" name="remarks" placeholder="Enter the Remarks..."
                                           value="remarks">

                                </div>
                            </div>

                        </div>

                    </div>

                    <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                        <h4 class="student_subhead">
                            <div class="paid_backcolors consignemt">Package Details</div>
                        </h4>
                    </div>

                    <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 padding_left_right_null">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 ">
                            <div class="sizes col-md-12 col-sm-12 col-xs-12 col-lg-12 padding_left_right_null">
                                <input type="radio" onclick="javascript:yesnoCheck();" name="package" value="No Weight"
                                       id="noCheck"/>No Weight
                                <input type="radio" onclick="javascript:yesnoCheck();" name="package"
                                       value="Actual Weight" id="yesCheck"/>Actual Weight
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 padding_left_right_null">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padding_left_right_null">

                                    <div class="sizes"><strong>No of Pkgs:</strong>
                                    </div>
                                    <fieldset class="{{ $errors->has('pkgs') ? ' has-error' : '' }}">
                                        <input type="text" id="number1" name="pkgs"  placeholder="0" value="0">
                                        @if ($errors->has('pkgs'))<span
                                                class="help-block error_font"><strong>{{ $errors->first('pkgs') }}</strong></span>@endif
                                    </fieldset>
                                </div>

                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padding_left_right_null">

                                    <div class="sizes"><strong>Type of Packing:</strong></div>
                                    <fieldset class="{{ $errors->has('typeofpacking') ? ' has-error' : '' }}">
                                        <select id="typeofpacking" name="typeofpacking">
                                            <option value="">Type of Packing</option>
                                            <option value="Auto Parts">Auto Parts</option>
                                            <option value="BAG">BAG</option>
                                            <option value="Bandal">Bandal</option>
                                            <option value="BARREL">BARREL</option>
                                            <option value="Battery">Battery</option>
                                            <option value="Battery">Battery Case</option>
                                            <option value="BED">BED</option>
                                            <option value="Bill">Bill</option>
                                            <option value="Bitts">Bitts</option>
                                            <option value="Blue Bundle">Blue Bundle</option>
                                            <option value="Box">Box</option>
                                            <option value="Butti">Butti</option>
                                            <option value="Can(Empty)">Can(Empty)</option>
                                            <option value="Cann">Cann</option>
                                            <option value="CARTON">CARTON</option>
                                            <option value="Chair">Chair</option>
                                            <option value="COMPUTER BOX">COMPUTER BOX</option>
                                            <option value="Cover">Cover</option>
                                            <option value="Die">Die</option>
                                            <option value="Drum">Drum</option>
                                            <option value="Engin Bloc">Engin Block</option>
                                            <option value="ENVELOPE">ENVELOPE</option>
                                            <option value="Fan Cartoon">Fan Cartoon</option>
                                            <option value="Food Item">Food Item</option>
                                            <option value="Glass">Glass</option>
                                            <option value="Fragile">Fragile</option>
                                            <option value="Gonny">Gonny</option>
                                            <option value="Gonny Bag Small">Gonny Bag Small</option>
                                            <option value="Green Plastic">Green Plastic</option>
                                            <option value="Hammer">Hammer</option>
                                            <option value="Long Bundle">Long Bundle</option>
                                            <option value="Long Pipe">Long Pipe</option>
                                            <option value="LOOSE">LOOSE</option>
                                            <option value="Machine">Machine</option>
                                            <option value="Mat">Mat</option>
                                            <option value="Metal Sheet">Metal Sheet</option>
                                            <option value="Motor">Motor</option>
                                            <option value="Others">Others</option>
                                            <option value="Packet">Packet</option>
                                            <option value="Parcel">Parcel</option>
                                            <option value="Patti">Patti</option>
                                            <option value="Pipe">Pipe</option>
                                            <option value="Ply">Ply</option>
                                            <option value="Rode Bundle">Rode Bundle</option>
                                            <option value="Role">Role</option>
                                            <option value="Rubber Mat">Rubber Mat</option>
                                            <option value="Saree Parcel">Saree Parcel</option>
                                            <option value="Scooter">Scooter</option>
                                            <option value="Scooty">Scooty</option>
                                            <option value="Scrape">Scrape</option>
                                            <option value="Sheet">Sheet</option>
                                            <option value="STONE">STONE</option>
                                            <option value="Table">Table</option>
                                            <option value="Tanki">Tanki</option>
                                            <option value="Tharmocol">Tharmocol Box</option>
                                            <option value="Thela (Bag)">Thela (Bag)</option>
                                            <option value="Tiles">Tiles</option>
                                            <option value="TIN">TIN</option>
                                            <option value="Tokri">Tokri</option>
                                            <option value="Tube">Tube</option>
                                            <option value="Tube Bandal">Tube Bandal</option>
                                            <option value="TV">TV</option>
                                            <option value="Two Wheeler">Two Wheeler</option>
                                            <option value="Tyre">Tyre</option>
                                            <option value="Tyre Bundle">Tyre Bundle</option>
                                            <option value="Valve">Valve</option>
                                            <option value="White Plastic">White Plastic</option>
                                            <option value="Wire Bundle">Wire Bundle</option>
                                            <option value="Wooden Bandal">Wooden Bandal</option>
                                            <option value="Wooden Box">Wooden Box</option>
                                        </select>
                                        @if ($errors->has('typeofpacking'))<span
                                                class="help-block error_font"><strong>{{ $errors->first('typeofpacking') }}</strong></span>@endif
                                    </fieldset>
                                </div>
                            </div>


                            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 padding_left_right_null padd_top_10 padd_buttom_20">

                                <div id="flipe"><strong> E-Waybill: </strong></div>
                                <div id="panele">
                                    <fieldset class="{{ $errors->has('ewaybill') ? ' has-error' : '' }}">
                                        <input type="text" id="ewaybill" name="ewaybill"
                                               placeholder="Enter the E-Waybill...." value="0">
                                        @if ($errors->has('ewaybill'))<span
                                                class="help-block error_font"><strong>{{ $errors->first('ewaybill') }}</strong></span>@endif
                                    </fieldset>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 padding_left_right_null padd_top_10">


                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 ">
                            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 ">
                                <div id="ifYes" style="visibility:hidden">
                                    <fieldset class="{{ $errors->has('weight') ? ' has-error' : '' }}">
                                        <span class="sizes"><strong>Weight:</strong></span>
                                        <input type="text" id="number3" name="weight" placeholder="0 kg" value="0">

                                        @if ($errors->has('weight'))<span
                                                class="help-block error_font"><strong>{{ $errors->first('weight') }}</strong></span>@endif
                                    </fieldset>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                                <span class="sizes"><strong>Amount:</strong></span>
                                <fieldset class="{{ $errors->has('amount') ? ' has-error' : '' }}">
                                    <input type="text" id="number2" name="amount"  placeholder="0"
                                           style="width: 100%">
                                    @if ($errors->has('amount'))<span
                                            class="help-block error_font"><strong>{{ $errors->first('amount') }}</strong></span>@endif
                                </fieldset>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                                <div class="sizes"><strong>Invoice Date: </strong>
                                </div>
                                <input type="date" id="dob" name="invoicedob">
                            </div>

                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">

                            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 padding_left_right_null padd_top_20 padd_buttom_20">

                                <div id="flipss"> Charges</div>
                                <div id="panelss">


                                    <div class="sizes"><strong>Loading (Rs):</strong>
                                    </div>

                                    <input type="text" id="loading" class="txt" name="loading"
                                           placeholder="0" value="0">


                                    <div class="sizes"><strong>Door Pickup (Rs):</strong></div>

                                    <input type="text" id="doorpickup" class="txt" name="doorpickup"
                                           placeholder="0" value="0">


                                    <div class="sizes"><strong>Door Delivery (Rs):</strong></div>

                                    <input type="text" id="doordelivery" name="doordelivery"
                                           placeholder="0" value="0">

                                    <div class="sizes"><strong>Extra (Rs):</strong>
                                    </div>

                                    <input type="text" id="extra" name="extra" placeholder="0"
                                           value="0">

                                </div>
                            </div>
                        </div>
                    </div>

                    <input type="button" value="Add" onClick="Multiply()">

                </div>

                <div class="button_submits col-md-12 col-sm-12 col-xs-12 col-lg-12">
                   <input type="submit" value="Submit">
                    <input type="reset" value="Clear">
                </div>
            </div>
        </div>
    </form>

    <div class="container-fluid">
        <div class="container table_size">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h6>Paid booking Details</h6>
                    <div class="container table_size">
                        <table bgcolor="#bdb76b" border ="8" class="table">
                            <thead>
                            <tr class="boldtable" bgcolor="#d3d3d3">
                                <td>Serial No</td>
                                <td>Booking Date </td>
                                <td>Consigner Name </td>
                                <td>Consigne Name </td>
                                <td>Destination City </td>
                                <td>Branch </td>
                                <td>Type of Packing </td>
                                <td>Pkgs</td>
                                <td>Weight </td>
                                <td>Amount</td>
                                <td>Freight </td>
                                <td>Grand Total </td>

                            </tr>
                            </thead>
                            @foreach ($users as $user)
                                <tr>
                                    <td>{{ $user->id }}</td>
                                    <td>{{ $user->invoicedob }}</td>
                                    <td>{{ $user->consignername }}</td>
                                    <td>{{ $user->congname }}</td>
                                    <td>{{ $user->destination }}</td>
                                    <td>{{ $user->branch }}</td>
                                    <td>{{ $user->typeofpacking }}</td>
                                    <td>{{ $user->pkgs }}</td>
                                    <td>{{ $user->weight }}</td>
                                    <td>{{ $user->amount }}</td>
                                    <td>{{ $user->freight }}</td>
                                    <td>{{ $user->grandtotal }}</td>
                                    <td> <a href ="{{ url('/check/'.$user->id) }}"><button class="buttons buttons2">Download</button></a></td>
                                </tr>
                            @endforeach
                        </table>
                    </div>

                    <table>
                        <tr>
                            <a href ="paid/paid_delete"><button class="buttons buttons2">Delete</button></a>
                            <a href ="paid/paid_edit"><button class="buttons buttons2">update</button></a>
                            {{--<a href ="/seleced"><button class="button button2">Select</button></a>--}}
                            </td>
                        </tr>
                    </table>

                </div>
            </div>
        </div>
    </div>
    <script>
        function Multiply() {
            var number1 = parseInt($('#number1').val());
            var number2 = parseInt($('#number2').val());
            var number3 = parseInt($('#number3').val());
            if(!number1){
                number1 = 1;
            }else if(!number2){
                number2 = 1;
            }else if(!number3){
                number3 = 1;
            }

            var output = number1 * number2 * number3;
            alert('Total = ' +output);

        }
    </script>


    <script>
        $(document).ready(function () {

            $(".txt").each(function () {
                $(this).keyup(function () {
                    calculateSum();
                });
            });

            $(".btn1").click(function () {
                $("p").hide();
            });

            $(".btn2").click(function () {
                $("p").show();
            });

            $("#flipe").click(function () {
                $("#panele").slideToggle("slow");
            });

            $("#flipss").click(function () {
                $("#panelss").slideToggle("slow");
            });

            $("#flip").click(function () {
                $("#panel").slideToggle("slow");
            });

            $("#flips").click(function () {
                $("#panels").slideToggle("slow");
            });

        });


    </script>


    <script type="text/javascript">

        function yesnoCheck() {
            if (document.getElementById('yesCheck').checked) {
                document.getElementById('ifYes').style.visibility = 'visible';
            } else {
                document.getElementById('ifYes').style.visibility = 'hidden';
            }
        }

        function calculateSum() {

            var sum = 0;
            //iterate through each textboxes and add the values
            $(".txt").each(function () {

                //add only if the value is number
                if (!isNaN(this.value) && this.value.length != 0) {
                    sum += parseFloat(this.value);
                }

            });
            //.toFixed() method will roundoff the final sum to 2 decimal places
            $("#sum").html(sum.toFixed(2));
        }

        function calc() {
            var textValue1 = document.getElementById('amount').value;
            var textValue2 = document.getElementById('pkgs').value;

            document.getElementById('freight').value = textValue1 * textValue2;
            document.getElementById('total').value = textValue1 * textValue2;
            document.getElementById('grandtotal').value = textValue1 * textValue2;
        }

    </script>

@endsection