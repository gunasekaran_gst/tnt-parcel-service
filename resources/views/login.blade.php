@extends('layout.main')
@section('title', 'Office Center in Krishnagiri')
@section('keywords', 'Office Center in Krishnagiri')
@section('description', 'Office Center in Krishnagiri')
@section('content')

    <form action="/login" method="post">
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

        <div class="container-fluid">
            <div class="container">
                    <div  class="col-md-12 col-sm-12 col-xs-12 col-lg-12 fpr padding_left_right_null padd_top_30 padd_buttom_30">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_top_30 padd_buttom_30">
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 agileits-top staff_form padd_top_30 padd_buttom_30 illustrator_home">
                            <form method="post" action="#">
                            <h3 class="logincontent" style="background-color:#383d46; text-align: center; text-shadow: 1px 0;">Log In</h3>
                            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 username form-group padd_top_30">
                                <label for="username">User Name:</label>
                                <fieldset class="{{ $errors->has('username') ? ' has-error' : '' }}">
                                    <input type="username" class="form-control" id="username" placeholder="Enter Username" name="username">
                                    @if ($errors->has('username'))<span
                                            class="help-block error_font"><strong>{{ $errors->first('username') }}</strong></span>@endif
                                    <h5>
                                        <center style="color:red;">
                                            @if(session()->has('message'))
                                                <div class="alert alert-success">
                                                    {{ session()->get('message') }}
                                                </div>
                                            @endif
                                        </center>

                                    </h5>
                                </fieldset>
                                </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 pwd form-group padd_top_30">
                                    <label for="pwd">Password:</label>
                                <fieldset class="{{ $errors->has('pwd') ? ' has-error' : '' }}">
                                    <input type="password" class="form-control" id="pwd" placeholder="Enter password" name="pwd">
                                    @if ($errors->has('pwd'))<span
                                            class="help-block error_font"><strong>{{ $errors->first('pwd') }}</strong></span>@endif

                                </fieldset>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 pwd checkbox">
                                    <label><input type="checkbox" name="remember"> Remember me</label>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 button_submitlog">
                                <input type="submit" value="Submit">&nbsp;
                            </div>

                            </form>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 ">
                        </div>
                    </div>
            </div>
        </div>
    </form>
@endsection

